//

import UIKit

class TableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var tableCell = "TableViewCell"
    
    var array = [Title]()
    var delegate:ViewController?
    var action : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: tableCell, bundle: nil), forCellReuseIdentifier: tableCell)
        
        
    }
    



}
extension TableViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCell, for: indexPath) as! TableViewCell
        cell.delegate = self
        cell.tableLabel.text = action
     
        return cell
    
    
    
}
}
