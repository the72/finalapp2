//
//  CollectionViewCell.swift
//  FinalApp2
//
//  Created by Nurba on 05.03.2021.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var collectLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var delegate:ViewController?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        }
    
    @IBAction func tapAdd(_ sender: Any) {
        addButton.setTitle("Added", for: .normal)
       
    }
    
    

}
